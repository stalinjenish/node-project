const mysql_client = require('../db/mysql');


module.exports.createDonor = (donor, callback) => {
  const query = `INSERT INTO blood_donor (donor_name, donor_email, donor_dob, blood_group, contact_number, alternate_contact_number, last_donated_date, stopped_donating) ` +
    `values ('${donor.donor_name}', '${donor.donor_email}', '${donor.donor_dob}', '${donor.blood_group}', '${donor.contact_number}', '${donor.alternate_contact_number}', '${donor.last_donated_date}', 0)`;

  mysql_client.raw(query)
    .then(function (result) {
      callback(null, result);
    }).catch(function (error) {
      callback(error, null);
    });
}

module.exports.getAllDonors = (callback) => {
  const query = `SELECT * FROM blood_donor`;
  mysql_client.raw(query)
    .then(function (result) {
      callback(null, result);
    }).catch(function (error) {
      callback(error, null);
    });
}

module.exports.getDonorById = (donor_id, callback) => {
  const query = `SELECT * FROM blood_donor WHERE donor_id=${donor_id}`;
  mysql_client.raw(query)
    .then(function (result) {
      callback(null, result);
    }).catch(function (error) {
      callback(error, null);
    });
}

module.exports.getDonorByEmail = (donor_email, callback) => {
  const query = `SELECT * FROM blood_donor WHERE donor_email='${donor_email}'`;
  mysql_client.raw(query)
    .then(function (result) {
      callback(null, result);
    }).catch(function (error) {
      callback(error, null);
    });
}

module.exports.updateDonor = (donor, callback) => {
  const query = `UPDATE blood_donor SET donor_name='${donor.donor_name}',donor_email='${donor.donor_email}',donor_dob='${donor.donor_dob}',blood_group='${donor.blood_group}',contact_number='${donor.contact_number}',alternate_contact_number='${donor.alternate_contact_number}',last_donated_date='${donor.last_donated_date}', stopped_donating=${donor.stopped_donating} WHERE donor_id=${donor.donor_id}`;
  mysql_client.raw(query)
    .then(function (result) {
      callback(null, result);
    }).catch(function (error) {
      callback(error, null);
    });
}

module.exports.deleteDonor = (donor_id, callback) => {
  const query = `DELETE FROM blood_donor WHERE donor_id=${donor_id}`;
  mysql_client.raw(query)
    .then(function (result) {
      callback(null, result);
    }).catch(function (error) {
      callback(error, null);
    });
}