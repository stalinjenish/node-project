const donorRepo = require('../../repositories/donor_repo');
var jwt = require('jwt-simple');
var userRepo = require('../../repositories/user_repo');
const secretKey = "stalin";

//Create Donor API
module.exports.createDonor = function (req, res) {
    res.setHeader('content-Type', 'application/json');
    var donor = req.body;
    donor.alternate_contact_number = donor.alternate_contact_number ? donor.alternate_contact_number : "";
    donor.last_donated_date = donor.last_donated_date ? donor.last_donated_date : "";
    if (isEmpty(donor.donor_name) || isEmpty(donor.donor_email) || isEmpty(donor.blood_group) || isEmpty(donor.donor_dob) || isEmpty(donor.contact_number)) {
        res.statusCode = 400;
        res.write(JSON.stringify({ "message": "Bad request." }));
        res.end();
    }
    donorRepo.getDonorByEmail(donor.donor_email, function (error, result) {
        if (error) {
            res.statusCode = 400;
            res.write(JSON.stringify(error));
            res.end();
        }
        if (result && result[0].length > 0) {
            res.statusCode = 400;
            res.write(JSON.stringify({ "message": "Email already exist." }));
            res.end();
        }
        else {
            donorRepo.createDonor(req.body, function (error, result) {
                if (error) {
                    res.statusCode = 400;
                    res.write(JSON.stringify(error));
                    res.end();
                }
                if (result) {
                    res.statusCode = 200;
                    res.write(JSON.stringify({ "message": "donor created successfully." }));
                    res.end();
                }
            });
        }
    });


}
//Get All Donor API
module.exports.getAllDonors = function (req, res) {
    res.setHeader('content-Type', 'application/json');

    donorRepo.getAllDonors(function (error, result) {
        if (error) {
            res.statusCode = 400;
            res.write(JSON.stringify(error));
            res.end();
        }
        if (result) {
            const records = result[0] ? result[0] : [];
            res.statusCode = 200;
            res.write(JSON.stringify(records));
            res.end();
        }
    });
}
//Get Donor By Id API
module.exports.getDonorById = function (req, res) {
    res.setHeader('content-Type', 'application/json');

    donorRepo.getDonorById(req.params.donor_id, function (error, result) {
        if (error) {
            res.statusCode = 400;
            res.write(JSON.stringify(error));
            res.end();
        }
        if (result) {
            const records = result[0][0] ? result[0][0] : {};
            res.statusCode = 200;
            res.write(JSON.stringify(records));
            res.end();
        }
    });
}
//Update Donor API
module.exports.updateDonor = function (req, res) {
    res.setHeader('content-Type', 'application/json');
    var donor = req.body;
    donor.alternate_contact_number = donor.alternate_contact_number ? donor.alternate_contact_number : "";
    donor.last_donated_date = donor.last_donated_date ? donor.last_donated_date : "";
    donor.stopped_donating = donor.stopped_donating ? donor.stopped_donating : 0;
    if (isEmpty(donor.donor_name) || isEmpty(donor.donor_email) || isEmpty(donor.blood_group) || isEmpty(donor.donor_dob) || isEmpty(donor.contact_number)) {
        res.statusCode = 400;
        res.write(JSON.stringify({ "message": "Bad request." }));
        res.end();
    }
    else {
        donorRepo.updateDonor(req.body, function (error, result) {
            if (error) {
                res.statusCode = 400;
                res.write(JSON.stringify(error));
                res.end();
            }
            if (result && result[0].affectedRows > 0) {
                res.statusCode = 200;
                res.write(JSON.stringify({ "message": "donor updated successfully" }));
                res.end();
            }
            else {
                res.statusCode = 400;
                res.write(JSON.stringify({ "message": "Invalid donor id" }));
                res.end();
            }
        });
    }
}
//Delete Donor API
module.exports.deleteDonor = function (req, res) {
    res.setHeader('content-Type', 'application/json');

    donorRepo.deleteDonor(req.params.donor_id, function (error, result) {
        if (error) {
            res.statusCode = 400;
            res.write(JSON.stringify(error));
            res.end();
        }
        if (result && result[0].affectedRows > 0) {
            res.statusCode = 200;
            res.write(JSON.stringify({ "message": "Donor deleted successfully" }));
            res.end();
        }
        else {
            res.statusCode = 400;
            res.write(JSON.stringify({ "message": "Invalid donor id" }));
            res.end();
        }
    });
}

function isEmpty(inputData) {
    return inputData == undefined || inputData == "";
}

function createUserSessionKey(userId, timeStamp) {
    return userId + '_@_' + timeStamp;
}
function decodeJWT(accessToken) {
    let payload = null;
    if (accessToken) {
        payload = jwt.decode(accessToken, secretKey);
    }

    return payload;
}



function generateJWT(user) {
    return jwt.encode({
        user: user,
        createdAt: new Date().getTime()
    }, secretKey);
}

function expiresIn(numDays) {
    var dateObj = new Date();
    return dateObj.setDate(dateObj.getDate() + numDays);
}

module.exports.login = function (req, res) {
    res.setHeader('content-Type', 'application/json');
    var email = req.body.email || '';
    var password = req.body.password || '';
    console.log(req.body);
    console.log(password);
    if (email === '' || password === '') {
        res.statusCode = 401;
        res.write(JSON.stringify({ "message": "Failed to authenticate. email or password cannot be null." }));
        res.end();
    }
    else {
        userRepo.findUserByEmail(email, function (error, result) {
            if (error) {
                res.statusCode = 401;
                res.write(JSON.stringify(error));
                res.end();
            }
            if (result && result[0].length > 0) {

                userRepo.authenticate(email, password, function (error, result) {

                    if (error) {
                        res.statusCode = 401;
                        res.write(JSON.stringify(error));
                        res.end();
                    }
                    else {
                        console.log(result[0]);
                        if (result[0].length > 0) {
                            const user = result[0];
                            const jwt = generateJWT(user);
                            console.log(jwt);
                            const payload = decodeJWT(jwt);
                            payload.accessToken = jwt;
                            const userLoginInfo = {
                                "user": payload.user,
                                "token": payload.accessToken,
                                "createdAt": payload.createdAt
                            };
                            res.statusCode = 200;
                            res.write(JSON.stringify(userLoginInfo));
                            res.end();
                        }
                        else {
                            res.statusCode = 401;
                            res.write(JSON.stringify({ "message": "Failed to authenticate. Please check  password." }));
                            res.end();
                        }
                    }

                });

            }
            else {
                res.statusCode = 401;
                res.write(JSON.stringify({ "message": "Failed to authenticate. Please check your email address and password." }));
                res.end();
            }
        });
    }
}












