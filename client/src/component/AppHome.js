
import React, { Component } from 'react';
import axios from "axios";
import DonorGrid from './DonorGrid';
import { setUserSession,apiURL } from '../utils/Common';
class AppHome extends Component {
  constructor(props){
    super(props);

    this.state = {
      alldonors: []
    }
  }

  componentDidMount() {
    console.log(`inside componentDidMount`);

    const that = this;
    setTimeout(function(){
      axios.get(apiURL+'api/donorGetAll').then(result => {

        console.log(result);
        const alldonors = (result && result.data) ? result.data : [];

        that.setState(prevState => ({
          alldonors: alldonors
        }));
        console.log(`alldonors: ${JSON.stringify(that.state.alldonors)}`);
    })});
}

render() {
  const {
      onReset,
      onRestart,
      onAdd: onAddItem,
      onDelete: onDeleteItem
  } = this.props;

      return (
        <div>
            <div>
              <h1>Total Donors : {this.state.alldonors.length}</h1>
              {/* <button
                className="btn btn-success m-2"
                onClick={onReset}
                disabled={this.state.alldonors.length === 0 ? "disabled" : ""}
              >
                <i className="fa fa-refresh" aria-hidden="true" />
              </button>
              <button
                className="btn btn-primary m-2"
                onClick={onRestart}
                disabled={this.state.alldonors.length !== 0 ? "disabled" : ""}
              >
                <i className="fa fa-recycle" aria-hidden="true" />
              </button> */}
            </div>
            <div>
                {
                this.state.alldonors.map(item => (
                    <DonorGrid
                        key={item.donor_id}
                        item={item}
                    />
                ))
              }
            </div>
        </div>
      );
  }
}
export default AppHome;