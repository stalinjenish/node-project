import React, { Component, props } from 'react';
import { NavLink, Link, useHistory } from 'react-router-dom';
import { removeUserSession, getToken } from '../utils/Common';
class Navigation extends Component {
     constructor(props) {
          super(props);
          this.state = { isLoggedIn: sessionStorage.getItem('token') != null && sessionStorage.getItem('token') != "" }
          this.setState(this.state)
     }
     handleLogout = () => {
          console.log("loo" + sessionStorage.getItem('token'))
          sessionStorage.removeItem('token');
          sessionStorage.removeItem('user');
          window.location.href = '/';
          // this.props.history.push('/');
     }
     render() {



          if (this.state.isLoggedIn == true) {
               return (
                    <div className="header">
                         <NavLink to="/" exact>Website Home page</NavLink>
                         <input type="button" value="Logout" onClick={this.handleLogout} />
                    </div>);
          }
          else {
               return (
                    <div className="header">
                         <NavLink to="/" exact>Website Home page</NavLink>
                         <NavLink to="/about" exact >About</NavLink>
                         <NavLink to="/login" exact>Login</NavLink>
                    </div>);
          }

     }
}
export default Navigation;