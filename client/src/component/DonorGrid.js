import React, { Component } from "react";

class DonorGrid extends Component {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-md-12">
            <span style={{ fontSize: 24 }} className={this.getBadgeClasses()}>
              {this.props.item.donor_id}
            </span>
            <span style={{ fontSize: 24 }} className={this.getBadgeClasses()}>
              {this.props.item.donor_name}
            </span>
            <span style={{ fontSize: 24 }} className={this.getBadgeClasses()}>
              {this.props.item.donor_email}
            </span>
            <span style={{ fontSize: 24 }} className={this.getBadgeClasses()}>
              {this.props.item.blood_group}
            </span>
            <span style={{ fontSize: 24 }} className={this.getBadgeClasses()}>
              {this.props.item.contact_number}
            </span>
            <span style={{ fontSize: 24 }} className={this.getBadgeClasses()}>
              {this.props.item.alternate_contact_number}
            </span>
          </div>
        </div>
      </div>
    );
  }

  getBadgeClasses = () => {
    let classes = "badge m-2 badge-";
    classes += this.props.item.value === 0 ? "warning" : "primary";
    return classes;
  };

  formatCount = () => {
    const { value } = this.props.item;
    return value === 0 ? "Zero" : value;
  };
}

export default DonorGrid;
