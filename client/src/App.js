// import React from 'react';
// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;


import React, { Component } from 'react';
import { BrowserRouter as Router, Link, NavLink, Prompt } from 'react-router-dom';
import Route from 'react-router-dom/Route';
import Home from "./component/Home";
import About from "./component/About";
import Login from "./component/Login";
import AppHome from "./component/AppHome";
import Navigation from './component/Navigation';
class App extends Component {

  componentDidMount() 
  {
    this.state={isLoggedIn: sessionStorage.getItem('token') != null && sessionStorage.getItem('token') != ""}
  }
  

  render() {
    return (
      <Router>
        <div className="App">
          <Navigation {...this.state}>  </Navigation>
          {/* <h1><input type="text" value={`${this.state.isLoggedIn}`} readOnly></input></h1> */}
          <div className="content"> 
          <Route path="/" exact component={Home} />
          <Route path="/about" exact component={About} />
          <Route path="/login" exact component={Login} />
          <Route path="/apphome" exact component={AppHome} />
          </div>
         
        </div>
      </Router>
    );
  }
}

export default App;